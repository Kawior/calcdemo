package com.gkawka.calcDemo.solver.operations.mathoperation;

import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.NumericValue;
import com.gkawka.calcDemo.solver.operations.OperationPriority;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Should create tests for every math operation, but didn't have time so I created example test for root operation only.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 16.08.2018
 */
@RunWith(MockitoJUnitRunner.class)
public class RootOperationTest {

    private RootOperation objectUnderTest;

    private List<BigDecimal> testParameters;

    @Mock
    private NumericValue numericValue;

    @Before
    public void setup() {
        Bracket mainBracket = new Bracket();
        objectUnderTest = new RootOperation(mainBracket, mainBracket, numericValue);
    }

    @Test
    public void shouldReturnHighPriority() {
        assertEquals(OperationPriority.HIGH, objectUnderTest.getPriority());
    }

    @Test
    public void shouldReturnRootSymbol() {
        assertEquals(OperationSymbol.ROOT, objectUnderTest.getSymbol());
    }

    @Test
    public void shouldReturnCorrectRootResult() {
        testParameters = new ArrayList<>(Arrays.asList(new BigDecimal("2"), new BigDecimal("4")));
        BigDecimal result = objectUnderTest.calculateValue(testParameters);
        assertEquals("1.1892071150027210667", result.stripTrailingZeros().toPlainString());
    }

    @Test(expected = MathematicalOperationSyntaxException.class)
    public void shouldThrowMathematicalOperationSyntaxExceptionWhenRadicandIsLesserThenZero() {
        testParameters = new ArrayList<>(Arrays.asList(new BigDecimal("-2"), new BigDecimal("4")));
        objectUnderTest.calculateValue(testParameters);
    }

    @Test(expected = MathematicalOperationSyntaxException.class)
    public void shouldThrowMathematicalOperationSyntaxExceptionWhenThereIsOneParameter() {
        testParameters = new ArrayList<>(Collections.singletonList(new BigDecimal("4")));
        objectUnderTest.calculateValue(testParameters);
    }

    @Test(expected = MathematicalOperationSyntaxException.class)
    public void shouldThrowMathematicalOperationSyntaxExceptionWhenThereIsNoParameters() {
        testParameters = Collections.emptyList();
        objectUnderTest.calculateValue(testParameters);
    }

    @Test(expected = MathematicalOperationSyntaxException.class)
    public void shouldThrowMathematicalOperationSyntaxExceptionWhenParametersListIsNull() {
        objectUnderTest.calculateValue(null);
    }

    @Test(expected = MathematicalOperationSyntaxException.class)
    public void shouldThrowMathematicalOperationSyntaxExceptionWhenParameterIsNull() {
        objectUnderTest.calculateValue(new ArrayList<>(Collections.singletonList(null)));
    }

    @Test(expected = MathematicalOperationSyntaxException.class)
    public void shouldThrowMathematicalOperationSyntaxExceptionWhenParameterAreNull() {
        objectUnderTest.calculateValue(new ArrayList<>(Arrays.asList(null, null)));
    }
}
