package com.gkawka.calcDemo.solver;

import com.gkawka.calcDemo.solver.parser.EquationParser;
import com.gkawka.calcDemo.solver.parser.formater.EquationFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class EquationSolverRegularEquationsParametrizedTest {

    private String fInput;
    private String fExpected;

    public EquationSolverRegularEquationsParametrizedTest(String fInput, String fExpected) {
        this.fInput = fInput;
        this.fExpected = fExpected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"2 ", "2"},
                {"2 ^ 2", "4"},
                {"2 ^ 8", "256"},
                {"( 2 / 3 ) * (((( 2 + 5 / 7 + 3 ))) ^ 3 - 2 / 7 )", "124.202138"},
                {"( 2 / 3 ) * (((( 2 + 5 / 7 + 3 ))) ^ 3 ^ 2 - 2 / 7 )", "23210.093221"},
                {"2 + 2", "4"},
                {"2 + 1 + 3", "6"},
                {"2 + 45 × 2", "92"},
                {"3 × 1 × 4 + 1", "13"},
                {"(( 2 + 2 ) * 2 ) * 2", "16"},
                {"( 2 + 2 ) + 2 * 2", "8"},
                {"( 8 - 1 + 3 ) * 6 - (( 3 + 7 ) * 2 )", "40"},
                {"( 8 - 1 + 3 ) * 6 ^ (( 3 + 7 ) - 2 )", "16796160"},
                {"3 - 1", "2"},
                {"8 - 1 + 3", "10"},
                {"5 - 2 + 4 * ( 8 - ( 5 + 1 )) + 9", "20"},
                {"2 / 3", "0.666667"},
                {"( 2 / 3 ) * ( 2 + 5 / 7 )", "1.809524"},
                {"( 2 / 3 ) * ( 2 + 5 / 7 + 3 - 2 / 7 )", "3.619048"},
                {"( 2 / 3 ) * (((( 2 + 5 / 7 + 3 ))) + 3 - 2 / 7 )", "5.619048"},
                {"1 / 2 / 3 / 4 / ( 2 + 3 )", "0.008333"},
                {"64 rt 2", "8"},
                {"root ( 3 , 4 )", "1.316074"},
                {"root ( root ( 3 , 4 ) , 4 )", "1.071075"},
                {"3 - root ( 3 , 4 ) + 4", "5.683926"},
                {"( 2 * 34 ) + 12 + ( 21 - 6 )", "95"}
        });
    }

    @Before
    public void setup() {

    }

    @Test
    public void shouldSolveRegularEquations() {
        EquationSolver objectUnderTest = new EquationSolver(new EquationParser(), new EquationFormatter());
        assertEquals(fExpected, objectUnderTest.solve(fInput));
    }

}
