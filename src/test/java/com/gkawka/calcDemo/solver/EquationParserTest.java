package com.gkawka.calcDemo.solver;

import com.gkawka.calcDemo.solver.parser.EquationParser;
import com.gkawka.calcDemo.solver.parser.ParsedEquationSymbol;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 16.08.2018
 */
@RunWith(MockitoJUnitRunner.class)
public class EquationParserTest {

    @Test
    public void shouldParseSimpleEquationCorrectly() {
        List<ParsedEquationSymbol> parsedList = new EquationParser().parse("2 + 2");
        assertNotNull(parsedList);
        assertEquals(3, parsedList.size());
        assertEquals("2", parsedList.get(0).getValue());
        assertEquals("+", parsedList.get(1).getValue());
        assertEquals("2", parsedList.get(2).getValue());
    }

    @Test
    public void shouldParseComplicatedEquationCorrectly() {
        List<ParsedEquationSymbol> parsedList = new EquationParser().parse("2 + 2 * root ( 1 , 3 ) + ( 7 - 3 * 4 )");
        assertNotNull(parsedList);
        assertEquals(17, parsedList.size());
        assertEquals("2", parsedList.get(0).getValue());
        assertEquals("+", parsedList.get(1).getValue());
        assertEquals("2", parsedList.get(2).getValue());
        assertEquals("*", parsedList.get(3).getValue());
        assertEquals("root", parsedList.get(4).getValue());
        assertEquals("1", parsedList.get(5).getValue());
        assertEquals(",", parsedList.get(6).getValue());
        assertEquals("3", parsedList.get(7).getValue());
        assertEquals(ParsedEquationSymbol.SymbolType.CLOSE_BRACKET, parsedList.get(8).getSymbolType());
        assertEquals("+", parsedList.get(9).getValue());
        assertEquals(ParsedEquationSymbol.SymbolType.OPEN_BRACKET, parsedList.get(10).getSymbolType());
        assertEquals("7", parsedList.get(11).getValue());
        assertEquals("-", parsedList.get(12).getValue());
        assertEquals("3", parsedList.get(13).getValue());
        assertEquals("*", parsedList.get(14).getValue());
        assertEquals("4", parsedList.get(15).getValue());
        assertEquals(ParsedEquationSymbol.SymbolType.CLOSE_BRACKET, parsedList.get(16).getSymbolType());
    }

    @Test(expected = EquationSyntaxExcetpion.class)
    public void shouldThrowEquationSyntaxExceptionIfEquationTextIsIncorrect() {
        new EquationParser().parse("2+2o");

    }

}
