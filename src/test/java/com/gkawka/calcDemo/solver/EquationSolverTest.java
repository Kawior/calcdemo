package com.gkawka.calcDemo.solver;

import com.gkawka.calcDemo.solver.parser.EquationParser;
import com.gkawka.calcDemo.solver.parser.formater.EquationFormatter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 16.08.2018
 */
@RunWith(MockitoJUnitRunner.class)
public class EquationSolverTest {

    @Test(expected = EquationSyntaxExcetpion.class)
    public void shouldReturnEquationSyntaxExceptionWhenEquationHasUnknownSymbol() {
        new EquationSolver(new EquationParser(), new EquationFormatter()).solve("2+dasde");
    }

}
