package com.gkawka.calcDemo.solver;

import com.gkawka.calcDemo.solver.parser.EquationParser;
import com.gkawka.calcDemo.solver.parser.formater.EquationFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class EquationSolverExponentialFunctionIntegralParametrizedTest {

    private String fInput;
    private String fExpected;

    public EquationSolverExponentialFunctionIntegralParametrizedTest(String fInput, String fExpected) {
        this.fInput = fInput;
        this.fExpected = fExpected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"integral ( 1 , 2 , 8 , 10000 )", "4.671"},
//                {"3 - integral ( 1 , 2 , 8 , 10000 ) * 7", "-29.694"},
//                {"integral ( 1 , 2 , 8 , 10000 ) + 3", "7.671"}
        });
    }

    @Before
    public void setup() {

    }

    @Test
    public void shouldSolveExponentialFunctionIntegralEquations() {
        EquationSolver objectUnderTest = new EquationSolver(new EquationParser(), new EquationFormatter());
        assertEquals(fExpected, new BigDecimal(objectUnderTest.solve(fInput)).setScale(3, RoundingMode.HALF_EVEN).stripTrailingZeros().toPlainString());
    }

}
