package com.gkawka.calcDemo.solver.parser.formater;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class EquationFormatterParametrizedTest {

    private String fInput;
    private String fExpected;

    public EquationFormatterParametrizedTest(String fInput, String fExpected) {
        this.fInput = fInput;
        this.fExpected = fExpected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"( 2 / 3) *(((( 2 + 5 /7+ 3 )))^3 - 2 /7)", "( 2 / 3 ) * (((( 2 + 5 / 7 + 3 ))) ^ 3 - 2 / 7 )"},
                {"(2/3)*((((2+5/7+3)))^3 ^ 2 - 2/     7 )", "( 2 / 3 ) * (((( 2 + 5 / 7 + 3 ))) ^ 3 ^ 2 - 2 / 7 )"}
        });
    }


    @Test
    public void shouldSolveRegularEquations() {
        assertEquals(fExpected, new EquationFormatter().format(fInput));
    }

}
