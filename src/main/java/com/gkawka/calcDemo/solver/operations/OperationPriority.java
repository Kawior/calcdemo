package com.gkawka.calcDemo.solver.operations;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Contains priorities of operations.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
@Getter
@AllArgsConstructor
public enum OperationPriority {
    BRACKET(4),
    HIGH(3),
    MEDIUM(2),
    LOW(1),
    UNDEFINED(0);

    private int level;

}
