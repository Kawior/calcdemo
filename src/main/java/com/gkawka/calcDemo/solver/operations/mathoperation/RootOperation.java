package com.gkawka.calcDemo.solver.operations.mathoperation;

import ch.obermuhlner.math.big.BigDecimalMath;
import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElement;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;
import com.gkawka.calcDemo.solver.operations.OperationPriority;

import java.math.BigDecimal;
import java.util.List;

/**
 * Represents root mathoperation in the equation solver.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
public class RootOperation extends MathematicalOperation {

    RootOperation(EquationElementWithDependencies precedingOperation, Bracket parentBracket, EquationElement currentNumber) {
        super(precedingOperation, parentBracket, currentNumber);
    }

    /**
     * Calculates nth root.
     *
     * @param radicand radicand
     * @param index    index
     * @return nth root
     */
    public static BigDecimal calculateNthRoot(BigDecimal radicand, BigDecimal index) {
        if (radicand == null || index == null || radicand.intValue() < 0) {
            throw new MathematicalOperationSyntaxException("Can't root negative number: " + radicand);
        }
        return BigDecimalMath.root(radicand, index, EquationElementWithDependencies.mathContext);
    }

    /**
     * Performs root mathoperation on values.
     *
     * @param values calculated values from dependencies
     * @return root result
     */
    @Override
    protected BigDecimal calculateValue(List<BigDecimal> values) {
        if (values == null || values.size() < 2) {
            throw new MathematicalOperationSyntaxException("Incorrect root mathoperation. Not enough parameters.");
        }
        return values.stream().reduce(RootOperation::calculateNthRoot).orElseThrow(MathematicalOperationSyntaxException::new);
    }

    /**
     * Returns root mathoperation priority in the equation which is HIGH.
     *
     * @return HIGH priority
     */
    @Override
    public OperationPriority getPriority() {
        return OperationPriority.HIGH;
    }

    /**
     * Returns symbol of root mathoperation.
     *
     * @return root mathoperation symbol
     */
    @Override
    public OperationSymbol getSymbol() {
        return OperationSymbol.ROOT;
    }
}
