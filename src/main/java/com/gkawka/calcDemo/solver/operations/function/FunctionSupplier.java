package com.gkawka.calcDemo.solver.operations.function;


import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;

/**
 * Supplier for MathematicalFunction.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 12.08.2018
 */
@FunctionalInterface
public interface FunctionSupplier {

    MathematicalFunction apply(EquationElementWithDependencies operation, Bracket bracket);

}
