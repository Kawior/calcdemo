package com.gkawka.calcDemo.solver.operations;

import com.gkawka.calcDemo.solver.EquationSolvingException;
import com.gkawka.calcDemo.solver.EquationSyntaxExcetpion;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * Represents bracket in the equation solver.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
@Getter
@Setter
public class Bracket extends EquationElementWithDependencies {

    private EquationElementWithDependencies lastReadElement;

    /**
     * Creates root bracket. (no dependencies, no parent)
     */
    public Bracket() {
        this.lastReadElement = this;
    }

    public Bracket(EquationElementWithDependencies precedingOperation, Bracket parentBracket) {
        super(precedingOperation, parentBracket);
        this.lastReadElement = this;
    }

    /**
     * Performs calculation mathoperation on dependency. (should have only one)
     *
     * @param values calculated values from dependencies
     * @return bracket dependencies value
     */
    @Override
    protected BigDecimal calculateValue(List<BigDecimal> values) {
        if (values.size() == 0) {
            throw new EquationSyntaxExcetpion("Equation contains an empty bracket.");
        }
        if (values.size() > 1) {
            throw new EquationSolvingException("Bracket should have 1 dependency only.");
        }
        return values.stream().findAny().orElseThrow(() -> new EquationSolvingException("Error resolving bracket dependencies."));
    }

    /**
     * Return highest priority.
     *
     * @return highest priority
     */
    @Override
    public OperationPriority getPriority() {
        return OperationPriority.BRACKET;
    }

    /**
     * Returns parent element symbol.
     *
     * @return parent element symbol
     */
    @Override
    public Symbol getSymbol() {
        return getParentElement() != null ? getParentElement().getSymbol() : null;
    }

    public Bracket rollBackToParentBracket() {
        if (this.getParentElement() == null) {
            throw new EquationSyntaxExcetpion("Missing open bracket.");
        }
        this.getParentElement().getDependencies().remove(this);
        return this.getParentBracket();
    }
}
