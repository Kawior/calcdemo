package com.gkawka.calcDemo.solver.operations;

import com.gkawka.calcDemo.solver.EquationSyntaxExcetpion;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

/**
 * Represents number in the equation.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
@AllArgsConstructor
@Getter
public class NumericValue implements EquationElement {

    private BigDecimal value;

    public NumericValue(String equationSymbol) {
        try {
            this.value = new BigDecimal(equationSymbol);
        } catch (NumberFormatException nfe) {
            throw new EquationSyntaxExcetpion(equationSymbol + " is not a number.", nfe);
        }
    }

    /**
     * Returns the value of the number.
     *
     * @return value of number
     */
    @Override
    public BigDecimal solve() {
        return value;
    }
}
