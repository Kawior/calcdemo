package com.gkawka.calcDemo.solver.operations.function;

import com.gkawka.calcDemo.solver.EquationSyntaxExcetpion;
import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;
import com.gkawka.calcDemo.solver.operations.Symbol;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Arrays;

/**
 * Containing all function symbols and suppliers for constructors.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 12.08.2018
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum FunctionSymbol implements Symbol {
    ROOT(RootFunction::new, "root"),
    INTEGRAL(ExponentialFunctionIntegralFunction::new, "integral");

    private FunctionSupplier constructorSupplier;
    private String functionSymbol;

    /**
     * Returns FunctionSymbol object that symbol is equal to given String.
     *
     * @param symbol string symbol
     * @return FunctionSymbol
     */
    public static FunctionSymbol functionForSymbol(String symbol) {
        return Arrays.stream(FunctionSymbol.values())
                .filter(o -> o.functionSymbol.equals(symbol))
                .findFirst()
                .orElse(null);
    }

    /**
     * Returns MathematicalFunction object of given type.
     *
     * @param precedingOperation preceding mathoperation in the equation
     * @param parentBracket      bracket containing this object
     * @return MathematicalFunction
     */
    public MathematicalFunction getOperationConstructor(EquationElementWithDependencies precedingOperation, Bracket parentBracket) {
        if (constructorSupplier == null) {
            throw new EquationSyntaxExcetpion("Invalid function.");
        }
        return constructorSupplier.apply(precedingOperation, parentBracket);
    }

}
