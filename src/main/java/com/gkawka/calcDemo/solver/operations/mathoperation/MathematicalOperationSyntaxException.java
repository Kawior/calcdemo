package com.gkawka.calcDemo.solver.operations.mathoperation;

import com.gkawka.calcDemo.solver.EquationSyntaxExcetpion;

/**
 * Thrown when syntax of mathematical mathoperation is incorrect.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 15.08.2018
 */
public class MathematicalOperationSyntaxException extends EquationSyntaxExcetpion {
    public MathematicalOperationSyntaxException() {
        super();
    }

    public MathematicalOperationSyntaxException(String message) {
        super(message);
    }

    public MathematicalOperationSyntaxException(String message, Throwable cause) {
        super(message, cause);
    }

    public MathematicalOperationSyntaxException(Throwable cause) {
        super(cause);
    }

    protected MathematicalOperationSyntaxException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
