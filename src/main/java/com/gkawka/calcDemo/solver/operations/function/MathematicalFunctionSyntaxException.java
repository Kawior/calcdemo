package com.gkawka.calcDemo.solver.operations.function;

import com.gkawka.calcDemo.solver.EquationSyntaxExcetpion;

/**
 * Thrown when syntax of function mathoperation is incorrect.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 12.08.2018
 */
public class MathematicalFunctionSyntaxException extends EquationSyntaxExcetpion {

    public MathematicalFunctionSyntaxException() {
        super();
    }

    public MathematicalFunctionSyntaxException(String message) {
        super(message);
    }

    public MathematicalFunctionSyntaxException(String message, Throwable cause) {
        super(message, cause);
    }

    public MathematicalFunctionSyntaxException(Throwable cause) {
        super(cause);
    }

    protected MathematicalFunctionSyntaxException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
