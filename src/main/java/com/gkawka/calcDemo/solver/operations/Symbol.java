package com.gkawka.calcDemo.solver.operations;

/**
 * Represents mathematical or function symbol.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
public interface Symbol {
}
