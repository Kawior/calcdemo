package com.gkawka.calcDemo.solver.operations.function;

import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;
import com.gkawka.calcDemo.solver.operations.OperationPriority;
import com.gkawka.calcDemo.solver.operations.mathoperation.RootOperation;

import java.math.BigDecimal;
import java.util.List;

/**
 * Stands for nth root function in the equation.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
public class RootFunction extends MathematicalFunction {

    RootFunction(EquationElementWithDependencies precedingOperation, Bracket parentBracket) {
        super(precedingOperation, parentBracket, 2);
    }

    /**
     * Calculates nth root. First value in the values list is radicand and the second one is the root index.
     *
     * @param values radicand and root index
     * @return nth root
     */
    @Override
    protected BigDecimal calculateValue(List<BigDecimal> values) {
        return RootOperation.calculateNthRoot(values.get(0), values.get(1));
    }

    /**
     * Returns the priority of root mathoperation which is high.
     *
     * @return priority HIGH 3
     */
    @Override
    public OperationPriority getPriority() {
        return OperationPriority.HIGH;
    }

    /**
     * Returns the root symbol.
     *
     * @return root symbol
     */
    @Override
    public FunctionSymbol getSymbol() {
        return FunctionSymbol.ROOT;
    }
}
