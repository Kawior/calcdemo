package com.gkawka.calcDemo.solver.operations.function;

import ch.obermuhlner.math.big.BigDecimalMath;
import com.gkawka.calcDemo.solver.EquationSolvingException;
import com.gkawka.calcDemo.solver.EquationSyntaxExcetpion;
import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;
import com.gkawka.calcDemo.solver.operations.OperationPriority;
import com.gkawka.calcDemo.solver.utils.CompletableFutureUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Function resolving exponential function definite integral using Simpson's rule, in given range,
 * with given precision and using given number of threads.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
public class ExponentialFunctionIntegralFunction extends MathematicalFunction {

    ExponentialFunctionIntegralFunction(EquationElementWithDependencies precedingElement, Bracket parentBracket) {
        super(precedingElement, parentBracket, 4);
    }

    /**
     * Calculates definite integral for exponential function, using Simpsons rule.
     *
     * @param intervalStart    start of the interval
     * @param intervalEnd      end of the interval
     * @param threadCount      possible to use thread count
     * @param subIntervalCount number of sub intervals
     * @return integral value
     * @throws ExecutionException   if thread was aborted. not likely
     * @throws InterruptedException if thread was interrupted also not likely to happen
     */
    private static BigDecimal calculateExponentialFunctionIntegral(BigDecimal intervalStart, BigDecimal intervalEnd,
                                                                   BigDecimal threadCount, BigDecimal subIntervalCount)
            throws ExecutionException, InterruptedException {
        BigDecimal subIntervalSize = intervalEnd.subtract(intervalStart).divide(subIntervalCount, mathContext);

        BigDecimal subIntervalIntegralSum = calculateIntegralForAllSubIntervals(intervalStart, threadCount, subIntervalCount, subIntervalSize);

        return subIntervalSize.divide(new BigDecimal(6), mathContext)
                .multiply(calculateExponentialFunction(intervalStart)
                        .add(calculateExponentialFunction(intervalEnd))
                        .add((subIntervalIntegralSum)));
    }

    /**
     * Calculates sum of integrals on all sub intervals.
     *
     * @param intervalStart    start of the interval
     * @param threadCount      possible to use thread count
     * @param subIntervalCount number of sub intervals
     * @param subIntervalSize  size of one sub interval
     * @return sum of integrals on sub intervals
     */
    private static BigDecimal calculateIntegralForAllSubIntervals(BigDecimal intervalStart, BigDecimal threadCount,
                                                                  BigDecimal subIntervalCount, BigDecimal subIntervalSize) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(threadCount.intValue());

        List<CompletableFuture<BigDecimal>> subIntervalIntegralSumFutures = IntStream.range(0, subIntervalCount.intValue()).mapToObj(
                i -> CompletableFuture.supplyAsync(
                        () -> intervalStart.add(subIntervalSize.multiply(new BigDecimal(i))), executorService)
                        .thenApplyAsync(currentPosition -> calculateIntegralForSubInterval(currentPosition, subIntervalSize, i, subIntervalCount), executorService)
        ).collect(Collectors.toList());
        return executorService.submit(() -> CompletableFutureUtils
                .sequence(subIntervalIntegralSumFutures).get()
                .parallelStream()
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO)).get();
    }

    /**
     * Calculates integral value on sub interval.
     *
     * @param subIntervalStart  start point of sub interval
     * @param subIntervalSize   size of one sub interval
     * @param subIntervalNumber nth sub interval
     * @param subIntervalCount  number of all sub intervals
     * @return value of sub integral
     */
    private static BigDecimal calculateIntegralForSubInterval(BigDecimal subIntervalStart, BigDecimal subIntervalSize, int subIntervalNumber, BigDecimal subIntervalCount) {
        BigDecimal partialInterval = calculateExponentialFunction(subIntervalStart
                .subtract(subIntervalSize.divide(new BigDecimal(2), mathContext)))
                .multiply(new BigDecimal(4));
        if (subIntervalNumber < subIntervalCount.intValue()) {
            partialInterval = partialInterval
                    .add(calculateExponentialFunction(subIntervalStart)
                            .multiply(new BigDecimal(2)));
        }
        return partialInterval;
    }

    /**
     * Calculates the value of exponential function.
     *
     * @param power power argument
     * @return value of exponential function
     */
    private static BigDecimal calculateExponentialFunction(BigDecimal power) {
        return BigDecimalMath.pow(BigDecimalMath.e(mathContext), power, mathContext);
    }

    /**
     * Calls calculateExponentialFunctionIntegral on first 4 values. Throws EquationSolvingException if threads are interrupted.
     *
     * @param values values of dependent elements
     * @return calculated integral
     */
    @Override
    protected BigDecimal calculateValue(List<BigDecimal> values) {
        if(numberOfParameters() != getExpectedParameters()) {
            throw new EquationSyntaxExcetpion("Incorrect number of parameters, expected: " + getExpectedParameters());
        }
        try {
            return calculateExponentialFunctionIntegral(values.get(0), values.get(1), values.get(2), values.get(3));
        } catch (ExecutionException | InterruptedException e) {
            throw new EquationSolvingException("Error calculating exponential function integral. ", e);
        }
    }

    /**
     * Returns function priority.
     *
     * @return function priority
     */
    @Override
    public OperationPriority getPriority() {
        return OperationPriority.HIGH;
    }

    /**
     * Returns integral symbol.
     *
     * @return integral symbol
     */
    @Override
    public FunctionSymbol getSymbol() {
        return FunctionSymbol.INTEGRAL;
    }
}
