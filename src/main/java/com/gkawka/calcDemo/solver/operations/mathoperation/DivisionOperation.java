package com.gkawka.calcDemo.solver.operations.mathoperation;

import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElement;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;
import com.gkawka.calcDemo.solver.operations.OperationPriority;

import java.math.BigDecimal;
import java.util.List;

/**
 * Represents division mathoperation.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
public class DivisionOperation extends MathematicalOperation {

    DivisionOperation(EquationElementWithDependencies precedingOperation, Bracket parentBracket, EquationElement currentNumber) {
        super(precedingOperation, parentBracket, currentNumber);
    }

    /**
     * Calculates n - 1 divisions of n values.
     *
     * @param values calculated values from dependencies
     * @return division result
     */
    @Override
    protected BigDecimal calculateValue(List<BigDecimal> values) {
        if (values == null || values.size() < 2) {
            throw new MathematicalOperationSyntaxException("Incorrect division mathoperation. Not enough parameters.");
        }
        return values.stream().reduce(this::divide)
                .orElseThrow(() -> new MathematicalOperationSyntaxException("Incorrect division mathoperation."));
    }

    /**
     * Calculates separate division.
     *
     * @param dividend dividend
     * @param divider  divider
     * @return division result
     */
    private BigDecimal divide(BigDecimal dividend, BigDecimal divider) {
        if (BigDecimal.ZERO.equals(divider)) {
            throw new MathematicalOperationSyntaxException("Can't divide by 0.");
        }
        return dividend.divide(divider, mathContext);
    }

    /**
     * Returns division mathoperation priority in the equation which is MEDIUM.
     *
     * @return MEDIUM priority
     */
    @Override
    public OperationPriority getPriority() {
        return OperationPriority.MEDIUM;
    }

    /**
     * Returns symbol of division.
     *
     * @return division symbol
     */
    @Override
    public OperationSymbol getSymbol() {
        return OperationSymbol.DIVISION;
    }
}
