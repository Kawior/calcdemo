package com.gkawka.calcDemo.solver.operations.mathoperation;

import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElement;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;

/**
 * Represents every mathematical mathoperation in the equation.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 12.08.2018
 */
public abstract class MathematicalOperation extends EquationElementWithDependencies {

    MathematicalOperation() {
        super();
    }

    /**
     * Defines the priority of the mathoperation relatively to preceding mathoperation.
     *
     * @param precedingOperation preceding mathoperation in the equation
     * @param parentBracket      bracket containing this mathoperation
     * @param lastReadNumber     last read number
     */
    MathematicalOperation(EquationElementWithDependencies precedingOperation, Bracket parentBracket, EquationElement lastReadNumber) {
        super(precedingOperation, parentBracket);
        parentBracket.setLastReadElement(this);
        if (this.hasHigherPriorityThanOtherElement(precedingOperation) || precedingOperation.equals(parentBracket)) {
            this.addDependency(lastReadNumber);
        } else {
            precedingOperation.addDependency(lastReadNumber);
        }
    }

    /**
     * Returns symbol of mathoperation.
     *
     * @return mathoperation symbol
     */
    @Override
    public abstract OperationSymbol getSymbol();
}
