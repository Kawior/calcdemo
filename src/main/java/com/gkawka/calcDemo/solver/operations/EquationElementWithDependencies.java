package com.gkawka.calcDemo.solver.operations;

import lombok.Getter;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class stands for every mathoperation that can require another mathoperation to be solved before solving.
 * Basically this can be anything in the equation except numbers.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
@Getter
public abstract class EquationElementWithDependencies implements EquationElement {

    public static MathContext mathContext = new MathContext(20, RoundingMode.HALF_EVEN);

    protected EquationElementWithDependencies parentElement;
    protected Bracket parentBracket;
    private List<EquationElement> dependencies;

    /**
     * No args constructor required for elements that act different in the equation, like brackets or functions.
     */
    public EquationElementWithDependencies() {
        this.dependencies = new ArrayList<>();
    }

    /**
     * Constructor for typical equation elements, like mathematical operations. Sets the parent bracket for the element.
     * Makes element dependency or parent of preceding equation element.
     *
     * @param precedingElement preceding element in the equation
     * @param parentBracket    the bracket containing this element
     */
    public EquationElementWithDependencies(EquationElementWithDependencies precedingElement, Bracket parentBracket) {
        this.parentBracket = parentBracket;
        this.dependencies = new ArrayList<>();

        if (this.hasHigherPriorityThanOtherElement(precedingElement) || precedingElement.equals(parentBracket)) {
            makeDependencyOf(precedingElement);
        } else {
            makeParentOf(precedingElement);
        }
    }

    /**
     * Makes this dependency of another element.
     *
     * @param element parent element for this
     */
    protected void makeDependencyOf(EquationElementWithDependencies element) {
        this.parentElement = element;
        element.addDependency(this);
    }

    /**
     * Makes this parent of another element.
     *
     * @param element dependency element
     */
    private void makeParentOf(EquationElementWithDependencies element) {
        this.parentElement = element.getParentElement();
        this.parentElement.dependencies.remove(element);
        this.parentElement.dependencies.add(this);
        element.parentElement = this;
        this.addDependency(element);
    }

    /**
     * Calls recursively solve on every dependency of this function and then calculates the value of element.
     *
     * @return value of the element
     */
    @Override
    public BigDecimal solve() {
        return calculateValue(dependencies.stream().map(EquationElement::solve).collect(Collectors.toList()));
    }

    /**
     * Compares the priority of this element with another element.
     *
     * @param otherElement compared element
     * @return true if this has higher priority
     */
    protected boolean hasHigherPriorityThanOtherElement(EquationElementWithDependencies otherElement) {
        return this.getPriority().getLevel() > otherElement.getPriority().getLevel();
    }

    /**
     * Adds dependency if not null.
     *
     * @param element dependency
     */
    public void addDependency(EquationElement element) {
        if (element != null) {
            dependencies.add(element);
        }
    }

    /**
     * Abstract method returning the priority of element.
     *
     * @return priority
     */
    public abstract OperationPriority getPriority();

    /**
     * Abstract method returning the Symbol of the element. Symbol is an interface implemented by enums containing data,
     * regarding mathoperation or function symbol and supplying constructors.
     *
     * @return Symbol of element
     */
    public abstract Symbol getSymbol();

    /**
     * Contains set of calculations to be performed on list of values calculated from element dependencies.
     * It evaluates the mathoperation.
     *
     * @param values calculated values from dependencies
     * @return calculated value of element
     */
    protected abstract BigDecimal calculateValue(List<BigDecimal> values);

}
