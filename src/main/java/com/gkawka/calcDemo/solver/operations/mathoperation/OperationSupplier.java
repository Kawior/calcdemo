package com.gkawka.calcDemo.solver.operations.mathoperation;


import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElement;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;

/**
 * Supplier for MathematicalOperation class.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 12.08.2018
 */
@FunctionalInterface
public interface OperationSupplier {

    MathematicalOperation apply(EquationElementWithDependencies operation, Bracket bracket, EquationElement equationElement);

}
