package com.gkawka.calcDemo.solver.operations.mathoperation;

import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElement;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;
import com.gkawka.calcDemo.solver.operations.OperationPriority;

import java.math.BigDecimal;
import java.util.List;

/**
 * Represents subtraction mathoperation in equation solver.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
public class SubtractionOperation extends MathematicalOperation {

    SubtractionOperation(EquationElementWithDependencies precedingOperation, Bracket parentBracket, EquationElement currentNumber) {
        super(precedingOperation, parentBracket, currentNumber);
    }

    /**
     * Performs subtraction on values.
     *
     * @param values calculated values from dependencies
     * @return subtraction result
     */
    @Override
    protected BigDecimal calculateValue(List<BigDecimal> values) {
        if (values == null || values.size() < 1) {
            throw new MathematicalOperationSyntaxException("Incorrect subtraction mathoperation. Not enough parameters.");
        }
        if (values.size() == 1) {
            return values.get(0).negate();
        }
        return values.stream().reduce(BigDecimal::subtract)
                .orElseThrow(() -> new MathematicalOperationSyntaxException("Incorrect subtraction mathoperation. Not enough parameters."));
    }

    /**
     * Returns subtraction priority in the equation which is LOW.
     *
     * @return LOW priority
     */
    @Override
    public OperationPriority getPriority() {
        return OperationPriority.LOW;
    }

    /**
     * Returns symbol of subtraction.
     *
     * @return subtraction symbol
     */
    @Override
    public OperationSymbol getSymbol() {
        return OperationSymbol.SUBTRACTION;
    }
}
