package com.gkawka.calcDemo.solver.operations.mathoperation;

import ch.obermuhlner.math.big.BigDecimalMath;
import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElement;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;
import com.gkawka.calcDemo.solver.operations.OperationPriority;

import java.math.BigDecimal;
import java.util.List;

/**
 * Represents power mathoperation in the equation solver.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
public class PowerOperation extends MathematicalOperation {

    PowerOperation(EquationElementWithDependencies precedingOperation, Bracket parentBracket, EquationElement currentNumber) {
        super(precedingOperation, parentBracket, currentNumber);
    }

    /**
     * Calculates value of power mathoperation.
     *
     * @param values calculated values from dependencies
     * @return reult of power mathoperation
     */
    @Override
    protected BigDecimal calculateValue(List<BigDecimal> values) {
        if (values == null || values.size() < 2) {
            throw new MathematicalOperationSyntaxException("Incorrect power mathoperation. Not enough parameters.");
        }
        return values.stream()
                .reduce((v1, v2) -> BigDecimalMath.pow(v1, v2, EquationElementWithDependencies.mathContext))
                .orElseThrow(() -> new MathematicalOperationSyntaxException("Incorrect power mathoperation."));
    }

    /**
     * Returns power mathoperation priority in the equation which is HIGH.
     *
     * @return HIGH priority
     */
    @Override
    public OperationPriority getPriority() {
        return OperationPriority.HIGH;
    }

    /**
     * Returns symbol of power mathoperation.
     *
     * @return power mathoperation symbol
     */
    @Override
    public OperationSymbol getSymbol() {
        return OperationSymbol.POWER;
    }
}
