package com.gkawka.calcDemo.solver.operations;

import java.math.BigDecimal;

/**
 * Represents every equation element solver reads.
 */
public interface EquationElement {

    /**
     * Solving the element is returning its value.For mathematical mathoperation it will be result of mathoperation,
     * for number it will be its value.
     *
     * @return value
     */
    BigDecimal solve();
}
