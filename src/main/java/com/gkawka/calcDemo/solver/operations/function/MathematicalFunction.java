package com.gkawka.calcDemo.solver.operations.function;

import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElement;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;
import lombok.Data;

/**
 * Stands for every function in the equation.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 12.08.2018
 */
@Data
public abstract class MathematicalFunction extends Bracket {

    private int expectedParameters;

    private MathematicalFunction parentFunction;

    /**
     * Function is always dependent on preceding element.
     *
     * @param precedingElement   preceding element
     * @param parentBracket      bracket containing this function
     * @param expectedParameters maximal number of par
     */
    MathematicalFunction(EquationElementWithDependencies precedingElement, Bracket parentBracket, int expectedParameters) {
        super();
        makeDependencyOf(precedingElement);
        this.parentBracket = parentBracket;
        this.expectedParameters = expectedParameters;
    }

    /**
     * Return a function symbol which is function name.
     *
     * @return function name
     */
    @Override
    public abstract FunctionSymbol getSymbol();

    /**
     * Adds dependency to function, which is function parameter.
     *
     * @param element dependency function parameter
     */
    @Override
    public void addDependency(EquationElement element) {
        if (getDependencies().size() > this.expectedParameters) {
            throw new MathematicalFunctionSyntaxException("Too many parameters for function, expected: " + expectedParameters);
        }
        super.addDependency(element);
    }

    /**
     * Return current number of parameters.
     *
     * @return number of parameters
     */
    public int numberOfParameters() {
        return getDependencies().size();
    }
}
