package com.gkawka.calcDemo.solver.operations.mathoperation;

import com.gkawka.calcDemo.solver.EquationSyntaxExcetpion;
import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElement;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;
import com.gkawka.calcDemo.solver.operations.Symbol;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

/**
 * Containing all operations symbols and suppliers for constructors.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 08.08.2018
 */
@Getter
public enum OperationSymbol implements Symbol {
    ADDITION(AdditionOperation::new, "+"),
    MULTIPLICATION(MultiplicationOperation::new, "×", "*"),
    DIVISION(DivisionOperation::new, "÷", "/"),
    SUBTRACTION(SubtractionOperation::new, "-"),
    POWER(PowerOperation::new, "^"),
    ROOT(RootOperation::new, "rt");

    private List<String> symbols;
    private OperationSupplier constructor;

    OperationSymbol(OperationSupplier constructor, String... symbols) {
        this.constructor = constructor;
        this.symbols = Arrays.asList(symbols);
    }

    /**
     * Returns OperationSymbol enum that symbol is equal to given String.
     *
     * @param symbol string symbol
     * @return FunctionSymbol
     */
    public static OperationSymbol operationForSymbol(String symbol) {
        return Arrays.stream(OperationSymbol.values())
                .filter(o -> o.symbols.contains(symbol))
                .findFirst()
                .orElse(null);
    }

    /**
     * Returns mathematical mathoperation symbol fo this kind.
     *
     * @param precedingOperation preceding mathoperation
     * @param parentBracket      bracket containing this mathoperation
     * @param lastReadNumber     last read number from the equation
     * @return new mathoperation
     */
    public MathematicalOperation getOperationConstructor(EquationElementWithDependencies precedingOperation, Bracket parentBracket, EquationElement lastReadNumber) {
        if (constructor == null) {
            throw new EquationSyntaxExcetpion("Invalid mathoperation.");
        }
        return constructor.apply(precedingOperation, parentBracket, lastReadNumber);
    }
}
