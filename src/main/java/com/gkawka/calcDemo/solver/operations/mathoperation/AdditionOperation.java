package com.gkawka.calcDemo.solver.operations.mathoperation;

import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElement;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;
import com.gkawka.calcDemo.solver.operations.OperationPriority;

import java.math.BigDecimal;
import java.util.List;

/**
 * Represents addition mathoperation.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
public class AdditionOperation extends MathematicalOperation {

    AdditionOperation(EquationElementWithDependencies precedingOperation, Bracket parentBracket, EquationElement currentNumber) {
        super(precedingOperation, parentBracket, currentNumber);
    }

    /**
     * Calculates addition of n values.
     *
     * @param values calculated values from dependencies
     * @return addition result
     */
    @Override
    protected BigDecimal calculateValue(List<BigDecimal> values) {
        if (values == null || values.size() < 2) {
            throw new MathematicalOperationSyntaxException("Incorrect addition mathoperation. Not enough parameters.");
        }
        return values.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * Returns addition priority in the equation which is LOW.
     *
     * @return LOW priority
     */
    @Override
    public OperationPriority getPriority() {
        return OperationPriority.LOW;
    }

    /**
     * Returns symbol of addition.
     *
     * @return addition symbol
     */
    @Override
    public OperationSymbol getSymbol() {
        return OperationSymbol.ADDITION;
    }
}
