package com.gkawka.calcDemo.solver.operations.mathoperation;

import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElement;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;
import com.gkawka.calcDemo.solver.operations.OperationPriority;

import java.math.BigDecimal;
import java.util.List;

/**
 * Represents multiplication mathoperation in the equation solver.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
public class MultiplicationOperation extends MathematicalOperation {

    MultiplicationOperation(EquationElementWithDependencies precedingOperation, Bracket parentBracket, EquationElement currentNumber) {
        super(precedingOperation, parentBracket, currentNumber);
    }

    /**
     * Calculates result of multiplication mathoperation.
     *
     * @param values calculated values from dependencies
     * @return multiplication result
     */
    @Override
    protected BigDecimal calculateValue(List<BigDecimal> values) {
        if (values == null || values.size() < 2) {
            throw new MathematicalOperationSyntaxException("Incorrect multiplication mathoperation. Not enough parameters.");
        }
        return values.stream().reduce(BigDecimal.ONE, BigDecimal::multiply);
    }

    /**
     * Returns multiplication priority in the equation which is MEDIUM.
     *
     * @return MEDIUM priority
     */
    @Override
    public OperationPriority getPriority() {
        return OperationPriority.MEDIUM;
    }

    /**
     * Returns symbol of multiplication.
     *
     * @return multiplication symbol
     */
    @Override
    public OperationSymbol getSymbol() {
        return OperationSymbol.MULTIPLICATION;
    }
}
