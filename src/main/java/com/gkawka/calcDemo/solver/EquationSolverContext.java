package com.gkawka.calcDemo.solver;

import com.gkawka.calcDemo.solver.operations.Bracket;
import com.gkawka.calcDemo.solver.operations.EquationElement;
import com.gkawka.calcDemo.solver.operations.EquationElementWithDependencies;
import com.gkawka.calcDemo.solver.operations.NumericValue;
import com.gkawka.calcDemo.solver.operations.function.FunctionSymbol;
import com.gkawka.calcDemo.solver.operations.function.MathematicalFunction;
import com.gkawka.calcDemo.solver.operations.mathoperation.OperationSymbol;
import com.gkawka.calcDemo.solver.parser.ParsedEquationSymbol;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 17.08.2018
 */
@Getter
@Setter
class EquationSolverContext {

    private List<ParsedEquationSymbol> equationSymbolList;
    private Bracket rootBracket;
    private Bracket currentBracket;
    private EquationElementWithDependencies lastReadOperation;
    private EquationElement lastReadNumber;


    /**
     * Sets up the defaults.
     */
    EquationSolverContext(List<ParsedEquationSymbol> equationSymbolList) {
        this.equationSymbolList = equationSymbolList;
        rootBracket = new Bracket();
        currentBracket = rootBracket;
        lastReadOperation = rootBracket;
        lastReadNumber = null;
    }

    /**
     * Adds element to equation elements tree.
     *
     * @param equationSymbol parsed symbol
     */
    void addElement(ParsedEquationSymbol equationSymbol) {
        if (equationSymbol.isOpenBracket()) {
            addBracket();
        } else if (equationSymbol.isNumber()) {
            lastReadNumber = new NumericValue(equationSymbol.getValue());
        } else if (equationSymbol.isMathematicalOperation()) {
            if (lastReadOperationSignEquals(equationSymbol.getValue())) {
                addNumberToLastReadOperation();
            } else {
                addOperation(equationSymbol);
            }
        } else if (equationSymbol.isCloseBracket()) {
            closeBracket();
        } else if (equationSymbol.isFunction()) {
            addFunction(equationSymbol);
        } else if (equationSymbol.isFunctionDelimiter()) {
            addArgumentToCurrentFunction();
        } else {
            throw new EquationSyntaxExcetpion("Unknown symbol: " + equationSymbol.getSymbolType());
        }
    }

    /**
     * Checks if next mathoperation in the equation has the same sign as lastReadOperation.
     *
     * @param nextOperationSign sign of next mathoperation
     * @return true if signs are the same
     */
    private boolean lastReadOperationSignEquals(String nextOperationSign) {
        return OperationSymbol.operationForSymbol(nextOperationSign).equals(lastReadOperation.getSymbol());
    }

    /**
     * Creates mathoperation and puts it into dependency tree from text.
     *
     * @param operationSymbol symbol of the mathoperation
     * @return mathoperation dependency tree element
     */
    private EquationElementWithDependencies createOperationFromSymbol(String operationSymbol) {
        return OperationSymbol.operationForSymbol(operationSymbol).getOperationConstructor(lastReadOperation, currentBracket, lastReadNumber);
    }

    /**
     * Creates function and puts it into dependency tree from text.
     *
     * @param functionSymbol function symbol
     * @return mathoperation dependency tree element
     */
    private MathematicalFunction createFunctionFromSymbol(String functionSymbol) {
        return FunctionSymbol.functionForSymbol(functionSymbol).getOperationConstructor(lastReadOperation, currentBracket);
    }

    /**
     * Adds argument to function. Every argument is in separate brackets. It simplifies parsing.
     */
    private void addArgumentToCurrentFunction() {
        closeBracket();
        MathematicalFunction currentFunction = (MathematicalFunction) lastReadOperation;
        currentFunction.addDependency(lastReadNumber);
        if (currentFunction.getExpectedParameters() - 1 > currentFunction.numberOfParameters()) {
            addBracket();
        }
    }

    /**
     * Adds function.
     *
     * @param equationSymbol function symbol
     */
    private void addFunction(ParsedEquationSymbol equationSymbol) {
        currentBracket = createFunctionFromSymbol(equationSymbol.getValue());
        lastReadOperation = currentBracket;
        addBracket();
    }

    /**
     * Adds bracket.
     */
    private void addBracket() {
        currentBracket = new Bracket(lastReadOperation, currentBracket);
        lastReadOperation = currentBracket;
    }

    /**
     * Closes bracket. Rolls it back to parent bracket.
     * And sets is as last read number, so it will be added to next mathoperation.
     */
    private void closeBracket() {
        lastReadOperation.addDependency(lastReadNumber);
        lastReadNumber = currentBracket;
        currentBracket = currentBracket.rollBackToParentBracket();
        lastReadOperation = currentBracket.getLastReadElement();
    }

    /**
     * Adds mathoperation and adds last read number to it.
     *
     * @param operationSymbol mathoperation symbol
     */
    private void addOperation(ParsedEquationSymbol operationSymbol) {
        lastReadOperation = createOperationFromSymbol(operationSymbol.getValue());
        lastReadNumber = null;
    }

    /**
     * Adds number to mathoperation.
     */
    private void addNumberToLastReadOperation() {
        lastReadOperation.addDependency(lastReadNumber);
    }

    BigDecimal solve() {
        lastReadOperation.addDependency(lastReadNumber);
        return rootBracket.solve();
    }
}
