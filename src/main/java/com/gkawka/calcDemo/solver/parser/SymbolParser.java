package com.gkawka.calcDemo.solver.parser;

import com.gkawka.calcDemo.solver.operations.mathoperation.OperationSymbol;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * Class parsing single char symbols.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 16.08.2018
 */
public class SymbolParser {

    /**
     * Checks if char is number
     *
     * @param ch char
     * @return true if is number
     */
    public static boolean isNumber(char ch) {
        return (ch >= '0' && ch <= '9') || ch == '.';
    }

    /**
     * Checks if char is bracket
     *
     * @param ch char
     * @return true if is bracket
     */
    public static boolean isBracket(char ch) {
        return ch == '(' || ch == ')';
    }

    /**
     * Checks if char is letter
     *
     * @param ch char
     * @return true if is letter
     */
    public static boolean isLetter(char ch) {
        return ch >= 'a' && ch <= 'z';
    }

    /**
     * Checks if char is function delimiter
     *
     * @param ch char
     * @return true if is function delimiter
     */
    public static boolean isFunctionDelimiter(char ch) {
        return ch == ',';
    }

    /**
     * Checks if char is mathematical mathoperation
     *
     * @param ch char
     * @return true if is mathematical mathoperation
     */
    public static boolean isMathematicalSymbol(char ch) {
        return new ArrayList<>(Arrays.asList(OperationSymbol.values()))
                .stream()
                .map(OperationSymbol::getSymbols)
                .flatMap(Collection::stream)
                .anyMatch(op -> op.length() == 1 && op.charAt(0) == ch);
    }

    /**
     * Checks if symbol is mathematical mathoperation
     *
     * @param symbol symbol in string
     * @return true if is mathematical mathoperation
     */
    public static boolean isMathematicalSymbol(String symbol) {
        return new ArrayList<>(Arrays.asList(OperationSymbol.values()))
                .stream()
                .map(OperationSymbol::getSymbols)
                .flatMap(Collection::stream)
                .anyMatch(op -> op.equals(symbol));
    }

}
