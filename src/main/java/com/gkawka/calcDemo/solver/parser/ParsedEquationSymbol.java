package com.gkawka.calcDemo.solver.parser;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents parsed symbol of the equation.
 * Contains set of factory methods for creating different types of symbols.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 11.08.2018
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ParsedEquationSymbol {

    private String value;
    private SymbolType symbolType;

    /**
     * Factory method creating mathoperation symbol.
     *
     * @param operationSymbol symbol
     * @return parsed mathoperation
     */
    static ParsedEquationSymbol createOperation(String operationSymbol) {
        ParsedEquationSymbol parsedEquationSymbol = new ParsedEquationSymbol();
        parsedEquationSymbol.symbolType = SymbolType.OPERATION;
        parsedEquationSymbol.value = operationSymbol;
        return parsedEquationSymbol;
    }

    /**
     * Factory method creating parsed number object.
     *
     * @param value number
     * @return parsed number
     */
    static ParsedEquationSymbol createNumber(String value) {
        ParsedEquationSymbol parsedEquationSymbol = new ParsedEquationSymbol();
        parsedEquationSymbol.symbolType = SymbolType.NUMBER;
        parsedEquationSymbol.value = value.replace("(", "").replace(")", "");
        return parsedEquationSymbol;
    }

    /**
     * Factory method creating parsed bracket.
     *
     * @param bracketType bracket type
     * @return parsed bracket
     */
    static ParsedEquationSymbol createBracket(int bracketType) {
        ParsedEquationSymbol parsedEquationSymbol = new ParsedEquationSymbol();
        if (bracketType == '(') {
            parsedEquationSymbol.symbolType = SymbolType.OPEN_BRACKET;
        } else if (bracketType == ')') {
            parsedEquationSymbol.symbolType = SymbolType.CLOSE_BRACKET;
        }
        return parsedEquationSymbol;
    }

    /**
     * Factory method creating function symbol.
     *
     * @param functionSymbol symbol
     * @return parsed function
     */
    static ParsedEquationSymbol createFunction(String functionSymbol) {
        ParsedEquationSymbol parsedEquationSymbol = new ParsedEquationSymbol();
        parsedEquationSymbol.setSymbolType(SymbolType.FUNCTION);
        parsedEquationSymbol.setValue(functionSymbol);
        return parsedEquationSymbol;
    }

    public boolean isFunctionDelimiter() {
        return SymbolType.FUNCTION_DELIMITER.equals(this.getSymbolType());
    }

    public boolean isFunction() {
        return SymbolType.FUNCTION.equals(this.getSymbolType());
    }

    public boolean isOpenBracket() {
        return SymbolType.OPEN_BRACKET.equals(this.getSymbolType());
    }

    public boolean isCloseBracket() {
        return SymbolType.CLOSE_BRACKET.equals(this.getSymbolType());
    }

    public boolean isNumber() {
        return SymbolType.NUMBER.equals(this.getSymbolType());
    }

    public boolean isMathematicalOperation() {
        return SymbolType.OPERATION.equals(this.getSymbolType());
    }

    public enum SymbolType {
        OPERATION,
        NUMBER,
        FUNCTION,
        OPEN_BRACKET,
        CLOSE_BRACKET,
        FUNCTION_DELIMITER
    }

}
