package com.gkawka.calcDemo.solver.parser.formater;

import com.gkawka.calcDemo.solver.parser.SymbolParser;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;

/**
 * Main purpose of this class is to change the input so every equation element is separated with space character.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 12.08.2018
 */
@Component
public class EquationFormatter {

    private static final char DELIMITER = ' ';

    /**
     * Method separating all equation element with delimiter.
     *
     * @return formatted equation text
     */
    public String format(String equationText) {
        StringBuilder formattedEquationStringBuilder = new StringBuilder(equationText.replace(" ", ""));

        if (formattedEquationStringBuilder.length() < 3) {
            return formattedEquationStringBuilder.toString();
        }
        for (int i = 0; i < formattedEquationStringBuilder.length() - 1; i++) {
            if (nextSymbolIsDifferentType(formattedEquationStringBuilder, i)) {
                insertDelimiterAt(formattedEquationStringBuilder, i + 1);
            }
        }
        return formattedEquationStringBuilder.toString();
    }

    /**
     * Checks if next symbol is of Different type.
     *
     * @param indexInEquationText index in equation text
     * @return true if next symbol is of Different type
     */
    private boolean nextSymbolIsDifferentType(StringBuilder stringBuilder, int indexInEquationText) {
        return thisSymbolIsOfTypeAndNextIsntOfType(this::isDigit, stringBuilder, indexInEquationText)
                || thisSymbolIsOfTypeAndNextIsntOfType(this::isBracket, stringBuilder, indexInEquationText)
                || thisSymbolIsOfTypeAndNextIsntOfType(this::isLetter, stringBuilder, indexInEquationText)
                || thisSymbolIsOfTypeAndNextIsntOfType(this::isMathematicalOperationSymbol, stringBuilder, indexInEquationText)
                || thisSymbolIsOfTypeAndNextIsntOfType(this::isFunctionDelimiter, stringBuilder, indexInEquationText);
    }

    /**
     * Checks if symbol on specific indexInEquationText in equation text is of specific type and next symbol is not of this type.
     *
     * @param symbolTypePredicate predicate specifying the type
     * @param indexInEquationText index in equation text
     * @return true if this symbol is of specified type and next is not
     */
    private boolean thisSymbolIsOfTypeAndNextIsntOfType(Predicate<Character> symbolTypePredicate, StringBuilder stringBuilder, int indexInEquationText) {
        return symbolTypePredicate.test(stringBuilder.charAt(indexInEquationText)) && !symbolTypePredicate.test(stringBuilder.charAt(indexInEquationText + 1));
    }

    /**
     * Puts delimiter symbol in equation text in specified position.
     *
     * @param indexInEquationText index in equation text to put delimiter
     */
    private void insertDelimiterAt(StringBuilder stringBuilder, int indexInEquationText) {
        stringBuilder.insert(indexInEquationText, EquationFormatter.DELIMITER);
    }

    /**
     * Check if symbol at index is digit.
     *
     * @param ch index in equation text
     * @return true if is digit
     */
    private boolean isDigit(char ch) {
        return SymbolParser.isNumber(ch);
    }

    /**
     * Check if symbol at index is letter.
     *
     * @param ch index in equation text
     * @return true if is letter
     */
    private boolean isLetter(char ch) {
        return SymbolParser.isLetter(ch);
    }

    /**
     * Check if symbol at index is bracket.
     *
     * @param ch index in equation text
     * @return true if is bracket
     */
    private boolean isBracket(char ch) {
        return SymbolParser.isBracket(ch);
    }

    /**
     * Check if symbol at index is function delimiter.
     *
     * @param ch index in equation text
     * @return true if is function delimiter
     */
    private boolean isFunctionDelimiter(char ch) {
        return SymbolParser.isFunctionDelimiter(ch);
    }

    /**
     * Check if symbol at index is mathematical mathoperation symbol.
     *
     * @param ch index in equation text
     * @return true if is mathematical mathoperation symbol
     */
    private boolean isMathematicalOperationSymbol(char ch) {
        return SymbolParser.isMathematicalSymbol(ch);
    }

}
