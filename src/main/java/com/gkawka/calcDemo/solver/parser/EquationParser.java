package com.gkawka.calcDemo.solver.parser;

import com.gkawka.calcDemo.solver.EquationSyntaxExcetpion;
import com.gkawka.calcDemo.solver.operations.function.FunctionSymbol;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Parsing equation formatted text int list of Parsed Symbols.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 11.08.2018
 */
@Component
public class EquationParser {

    private static final String DELIMITER = " ";
    private static final String FUNCTION_DELIMITER = ",";

    /**
     * Chacks if symbol is function.
     *
     * @param equationSymbol symbol
     * @return true if is function
     */
    private static boolean isFunction(String equationSymbol) {
        return FunctionSymbol.functionForSymbol(equationSymbol) != null;
    }

    /**
     * Chacks if symbol is function delimiter.
     *
     * @param equationSymbol symbol
     * @return true if is function delimiter
     */
    private static boolean isFunctionDelimiter(String equationSymbol) {
        return equationSymbol.chars().parallel().allMatch(ch -> SymbolParser.isFunctionDelimiter((char) ch));
    }

    /**
     * Chacks if symbol is number.
     *
     * @param equationSymbol symbol
     * @return true if is number
     */
    private static boolean isNumber(String equationSymbol) {
        return equationSymbol.chars().parallel().allMatch(ch -> SymbolParser.isNumber((char) ch));
    }

    /**
     * Chacks if symbol is bracket.
     *
     * @param equationSymbol symbol
     * @return true if is bracket
     */
    private static boolean isBracket(String equationSymbol) {
        return equationSymbol.chars().parallel().allMatch(ch -> SymbolParser.isBracket((char) ch));
    }

    /**
     * Chacks if symbol is mathoperation.
     *
     * @param equationSymbol symbol
     * @return true if is mathoperation
     */
    private static boolean isOperation(String equationSymbol) {
        return SymbolParser.isMathematicalSymbol(equationSymbol);
    }

    /**
     * Parses equation text. Throws exception if symbol is not recognised.
     *
     * @return list of symbols.
     */
    public List<ParsedEquationSymbol> parse(String equationText) {
        List<ParsedEquationSymbol> result = new LinkedList<>();
        boolean ignoreFunctionOpenBracket = false;

        for (String equationSymbol : equationText.split(DELIMITER)) {
            if (isOperation(equationSymbol)) {
                result.add(parseOperation(equationSymbol));
            } else if (isNumber(equationSymbol)) {
                result.add(parseNumber(equationSymbol));
            } else if (isBracket(equationSymbol)) {
                if (ignoreFunctionOpenBracket) {
                    equationSymbol = equationSymbol.replaceFirst("\\(", "");
                }
                ignoreFunctionOpenBracket = false;
                result.addAll(parseBrackets(equationSymbol));
            } else if (isFunction(equationSymbol)) {
                result.add(parseFunction(equationSymbol));
                ignoreFunctionOpenBracket = true;
            } else if (isFunctionDelimiter(equationSymbol)) {
                result.add(parseFunctionDelimiter());
            } else {
                throw new EquationSyntaxExcetpion("Unknown symbol: " + equationSymbol);
            }
        }
        return result;
    }

    /**
     * Adds function delimiter symbol to result list.
     */
    private ParsedEquationSymbol parseFunctionDelimiter() {
        return new ParsedEquationSymbol(FUNCTION_DELIMITER, ParsedEquationSymbol.SymbolType.FUNCTION_DELIMITER);
    }

    /**
     * Adds mathoperation symbol to result list.
     *
     * @param operationSymbol mathoperation symbol
     */
    private ParsedEquationSymbol parseOperation(String operationSymbol) {
        return ParsedEquationSymbol.createOperation(operationSymbol);
    }

    /**
     * Adds brackets if present. Removes first bracket if parsed function before.
     *
     * @param equationSymbol current read symbol
     */
    private List<ParsedEquationSymbol> parseBrackets(String equationSymbol) {
        return equationSymbol.chars().parallel().filter(ch -> ch == '(' || ch == ')').mapToObj(ParsedEquationSymbol::createBracket).collect(Collectors.toList());
    }

    /**
     * Adds Number to result list.
     *
     * @param number number
     */
    private ParsedEquationSymbol parseNumber(String number) {
        return ParsedEquationSymbol.createNumber(number);
    }

    /**
     * Adds function to result list.
     *
     * @param functionSymbol function symbol
     */
    private ParsedEquationSymbol parseFunction(String functionSymbol) {
        return ParsedEquationSymbol.createFunction(functionSymbol);
    }

}
