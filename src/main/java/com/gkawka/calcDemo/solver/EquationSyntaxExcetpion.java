package com.gkawka.calcDemo.solver;

/**
 * Thrown when syntax of the equation is incorrect.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 15.08.2018
 */
public class EquationSyntaxExcetpion extends RuntimeException {
    public EquationSyntaxExcetpion() {
    }

    public EquationSyntaxExcetpion(String message) {
        super(message);
    }

    public EquationSyntaxExcetpion(String message, Throwable cause) {
        super(message, cause);
    }

    public EquationSyntaxExcetpion(Throwable cause) {
        super(cause);
    }

    public EquationSyntaxExcetpion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
