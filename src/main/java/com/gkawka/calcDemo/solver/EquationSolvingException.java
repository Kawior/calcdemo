package com.gkawka.calcDemo.solver;

/**
 * Thrown when there is some unexpected problem on the application runtime.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
public class EquationSolvingException extends RuntimeException {
    public EquationSolvingException() {
        super();
    }

    public EquationSolvingException(String message) {
        super(message);
    }

    public EquationSolvingException(String message, Throwable cause) {
        super(message, cause);
    }
}
