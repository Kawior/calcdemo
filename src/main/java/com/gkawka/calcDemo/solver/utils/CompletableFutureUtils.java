package com.gkawka.calcDemo.solver.utils;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 12.08.2018
 */
public class CompletableFutureUtils {

    /**
     * Joins list of future into future of results.
     *
     * @param futureList list of futures
     * @param <T>        generic type
     * @return future of results.
     */
    public static <T> CompletableFuture<List<T>> sequence(List<CompletableFuture<T>> futureList) {
        return CompletableFuture.allOf(futureList.toArray(new CompletableFuture[futureList.size()]))
                .thenApply(v -> futureList.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList())
                );
    }
}
