package com.gkawka.calcDemo.solver;

import com.gkawka.calcDemo.solver.parser.EquationParser;
import com.gkawka.calcDemo.solver.parser.formater.EquationFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Creates dependency tree between operations and solves it.
 * The solution is similar to reverse polish notation but in oop.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
@Component
public class EquationSolver {

    private EquationParser equationParser;
    private EquationFormatter equationFormatter;

    @Autowired
    public EquationSolver(EquationParser equationParser, EquationFormatter equationFormatter) {
        this.equationParser = equationParser;
        this.equationFormatter = equationFormatter;
    }

    /**
     * Creates dependency tree between operations and solves the equation.
     *
     * @return equation result
     */
    public String solve(String equationText) {
        equationText = equationFormatter.format(equationText);

        EquationSolverContext equationSolverContext = new EquationSolverContext(equationParser.parse(equationText));
        equationSolverContext.getEquationSymbolList().forEach(equationSolverContext::addElement);

        return setResultScale(equationSolverContext.solve()).stripTrailingZeros().toPlainString();
    }

    /**
     * Sets scale of result BigDecimal.
     *
     * @param result unscaled result
     * @return scaled result
     */
    private static BigDecimal setResultScale(BigDecimal result) {
        return result.scale() > 6 ? result.setScale(6, RoundingMode.HALF_EVEN) : result;
    }





}


