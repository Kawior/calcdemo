package com.gkawka.calcDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
@SpringBootApplication
public class CalcDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CalcDemoApplication.class, args);
    }
}
