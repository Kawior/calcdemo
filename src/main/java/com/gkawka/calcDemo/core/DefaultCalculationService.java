package com.gkawka.calcDemo.core;

import com.gkawka.calcDemo.solver.EquationSolver;
import com.gkawka.calcDemo.solver.EquationSolvingException;
import com.gkawka.calcDemo.solver.EquationSyntaxExcetpion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Calculation service. Calling the solver tool. Managing the errors.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
@Service
class DefaultCalculationService implements CalculationService {

    private EquationSolver equationSolver;

    @Autowired
    public DefaultCalculationService(EquationSolver equationSolver) {
        this.equationSolver = equationSolver;
    }

    public CalculationResultDTO validateAndCalculateEquation(String equationText) {
        try {
            return new CalculationResultDTO(true, equationSolver.solve(equationText));
        } catch (EquationSolvingException e) {
            return CalculationResultDTO.createInvalidResponse("Internal error occurred.");
        } catch (EquationSyntaxExcetpion e) {
            return CalculationResultDTO.createInvalidResponse(e.getMessage());
        } catch (ArithmeticException e) {
            if ("Overflow".equals(e.getMessage())) {
                return CalculationResultDTO.createInvalidResponse("Result is too big.");
            }
            throw e;
        }
    }

}
