package com.gkawka.calcDemo.core;

/**
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
interface CalculationService {

    CalculationResultDTO validateAndCalculateEquation(String equationText);

}
