package com.gkawka.calcDemo.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Main REST controller with one method only for calculation.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
@RestController
class CalculationController {

    private CalculationService calculationService;

    @Autowired
    public CalculationController(CalculationService calculationService) {
        this.calculationService = calculationService;
    }

    /**
     * Calculating method.
     *
     * @param equationText equation text
     * @return calculation result or error
     */
    @RequestMapping(value = "/calculate", method = RequestMethod.POST)
    public ResponseEntity<CalculationResultDTO> calculate(@RequestBody String equationText) {
        return new ResponseEntity<>(calculationService.validateAndCalculateEquation(equationText), HttpStatus.OK);
    }

    /**
     * ExceptionHandler catching unexpected errors, so the stack trace is not presented to the user.
     *
     * @param e error
     * @return error
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<CalculationResultDTO> defaultExceptionHandler(Exception e) {
        e.printStackTrace();
        return new ResponseEntity<>(CalculationResultDTO.createInvalidResponse("Unknown error occurred."), HttpStatus.OK);
    }

}
