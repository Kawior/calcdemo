package com.gkawka.calcDemo.core;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Transport object for REST controller.
 * <p>
 * Created by Grzegorz Kawka <grzegorz.kawka@tlen.pl>
 * 05.08.2018
 */
@Data
@AllArgsConstructor
class CalculationResultDTO {

    private boolean isValid;
    private String result;

    static CalculationResultDTO createInvalidResponse(String cause) {
        return new CalculationResultDTO(false, cause);
    }

}
