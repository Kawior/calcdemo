angular.module('calcApp').service('FormatService', function () {
    this.format = function (text, callback) {

        for (var i = 0; i < text.length; i++) {
            if ((isDigitOrBracket(text[i]) && isOperator(text[i + 1])) || (isOperator(text[i]) && isDigitOrBracket(text[i + 1]))) {
                text = text.slice(0, i + 1) + " " + text.slice(i + 1);
                i++;
            }

            if (isBlankCharacter(text[i + 1]) && ((isDigit(text[i]) && isBracket(text[i + 2])) || (isBracket(text[i]) && isDigit(text[i + 2])))) {
                text = text.slice(0, i + 1) + text.slice(i + 2);
            }

            if ((isDigit(text[i]) && text[i + 1] === '(') || (text[i] === ')' && isDigit(text[i + 1]))) {
                text = text.slice(0, i + 1) + " × " + text.slice(i + 1);
                console.log(text)
            }

            while ((isBlankCharacter(text[i]) && isBlankCharacter(text[i + 1])) || isForbiddenBlankCharacter(text[i])) {
                text = text.slice(0, i) + text.slice(i + 1);
            }

        }
        callback(text);
    };

    this.validate = function (text) {
        for (var i = 0; i < text.length; i++) {
            if (isNaN(text[i]) && !isOperator(text[i])) {
                return false;
            }
        }
        return true;
    };

    function isDigitOrBracket(n) {
        return (isDigit(n) || isBracket(n));
    }

    function isDigit(n) {
        return (n >= '0' && n <= '9');
    }

    function isBracket(n) {
        return (n === '(' || n === ')');
    }


    function isBlankCharacter(char) {
        return char === ' ' || char === String.fromCharCode(160);
    }

    function isForbiddenBlankCharacter(char) {
        return char === String.fromCharCode(13) || char === String.fromCharCode(10) || char === String.fromCharCode(9);
    }

    function isOperator(char) {
        return char === '+'
            || char === '-'
            || char === '*'
            || char === '/'
            || char === String.fromCharCode(215) // times
            || char === String.fromCharCode(247) // divide
            || char === String.fromCharCode(8730) // radic
            || char === String.fromCharCode(8722) // minus
            || char === String.fromCharCode(43); // plus

    };

});