angular.module('calcApp').controller('CalcController', function ($scope, $timeout, $window, $mdToast, $mdDialog, CalcResource, FormatService) {
    $scope.equationText = "";
    $scope.localHistory = [];
    $scope.pointInLocalHistory = -1;
    $scope.fadeButtons = false;
    $scope.fadeHistoryButtons = true;
    $scope.historyButtonsHidden = true;
    $scope.invalidInput = false;

    $scope.showIntegralDialog = function (ev) {
        $mdDialog.show({
            controller: 'IntegralDialogController',
            templateUrl: 'app/public/calc/dialog/integral-dialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true
        })
            .then(function (answer) {
                $scope.equationText = answer;
                $scope.calculate();
                $scope.resizeTextArea();
            }, function () {
            });
    }

    $scope.showErrorToast = function (text) {
        var toast = $mdToast.simple()
            .textContent(text)
            .position('bottom center')
            .hideDelay(10000);
        $mdToast.show(toast);
    };

    $scope.openHistory = function () {
        $scope.fadeButtons = true;
        $scope.buttonsHidden = true;
        $timeout(function () {
            $scope.historyButtonsHidden = false;
            $scope.fadeHistoryButtons = false;
        }, 700);
    };

    $scope.closeHistory = function () {
        $scope.fadeHistoryButtons = true;
        $scope.historyButtonsHidden = true;
        $timeout(function () {
            $scope.buttonsHidden = false;
            $scope.fadeButtons = false;
        }, 700);
    };

    $scope.setEquationTextAndHideHistory = function (text) {
        $scope.equationText = text;
        $scope.closeHistory();
        $scope.resizeTextArea();
    };

    $scope.calculate = function () {

        FormatService.format($scope.equationText, function (result) {
            $scope.equationText = result;
            $scope.updateLocalHistory();
        });
        if ($scope.equationText === '') {
            $scope.showErrorToast('Input is empty.');
            $scope.invalidInput = true;
            return;
        }

        CalcResource.calculate({}, $scope.equationText).$promise.then(function (value) {
            if (value.valid === true) {
                $scope.equationText = value.result;
                $scope.updateLocalHistory();
                $scope.resizeTextArea();
            } else {
                $scope.showErrorToast(value.result);
                $scope.invalidInput = true;
            }
        });
    };

    $scope.undoFromLocalHistory = function () {

        if ($scope.pointInLocalHistory > 0) {
            $scope.pointInLocalHistory--;
        }
        $scope.equationText = $scope.localHistory[$scope.pointInLocalHistory].text;
        $scope.resizeTextArea();
    };

    $scope.redoFromLocalHistory = function () {

        if ($scope.pointInLocalHistory < $scope.localHistory.length - 1) {
            $scope.pointInLocalHistory++;
        }
        $scope.equationText = $scope.localHistory[$scope.pointInLocalHistory].text;
        $scope.resizeTextArea();
    };

    $scope.resizeTextArea = function () {
        jQuery.each(jQuery('textarea[data-autoresize]'), function () {
            var offset = this.offsetHeight - this.clientHeight;

            var resizeTextarea = function (el) {
                jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
            };
            resizeTextarea(this);
        });
    };

    $scope.updateLocalHistory = function () {
        $scope.invalidInput = false;
        $scope.pointInLocalHistory++;
        $scope.localHistory.push({id: $scope.pointInLocalHistory, text: $scope.equationText});
        $scope.resizeTextArea();
    };

    $scope.updateLocalHistory();

    $scope.erase = function () {
        $scope.equationText = "";
    };

    $scope.addSymbolToEquation = function (symbol) {
        $scope.equationText += symbol;
        $scope.updateLocalHistory();
    };

    $scope.getResultColorClass = function () {
        if ($scope.equationValidity === undefined) {
            return 'result-not-resolved';
        }
        return $scope.equationValidity === true ? 'result-success' : 'result-error';
    };

    $scope.validateEquation = function () {
        $scope.equationValidity = FormatService.validate($scope.equationText);
    };

    $scope.setFocusOnTextArea = function () {
        $timeout(function () {
            var element = $window.document.getElementById('equationInput');
            if (element)
                element.focus();
        });
    };


});