angular.module('calcApp').controller('IntegralDialogController', function ($scope, $mdDialog) {
    $scope.hide = function () {
        $mdDialog.hide();
    };

    $scope.cancel = function () {
        $mdDialog.cancel();
    };

    $scope.runCalculation = function (integral) {
        $mdDialog.hide('integral(' + integral.intervalStart + ', ' + integral.intervalEnd + ', ' + integral.threadCount + ', ' + integral.precision + ')');
    };
});