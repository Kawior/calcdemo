angular.module('calcApp').service('CalcResource', function ($resource, $rootScope) {
    return $resource('/calculate/:id', {
        id: '@id'
    }, {
        calculate: {
            method: 'POST'
        }
    });
});