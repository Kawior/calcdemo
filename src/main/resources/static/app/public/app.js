var calcApp = angular.module('calcApp', ['ngRoute', 'ngMaterial', 'ngAnimate', 'ngResource', 'ngMdIcons']);

calcApp.filter('reverse', function () {
    return function (items) {
        return items.slice().reverse();
    };
});




